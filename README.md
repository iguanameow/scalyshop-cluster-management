# Cluster management template project

Example [cluster management](https://docs.gitlab.com/ee/user/clusters/management_project.html) project.

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/cluster-management).


## Connecting Projects

+ ScalyShop Frontend - **[gitlab.com/iguanameow/scalyshop-frontend](https://gitlab.com/iguanameow/scalyshop-frontend)**
+ ScalyShop Backend - **[gitlab.com/iguanameow/scalyshop-backend](https://gitlab.com/iguanameow/scalyshop-backend)**
+ ScalyShop Christmas-App - **[gitlab.com/iguanameow/scalyshop-christmas-app](https://gitlab.com/iguanameow/scalyshop-christmas-app)**